#!/bin/sh

set -e

CHART="$1"

VERSION=$(grep -F "version:" "${CHART}/Chart.yaml" | cut -d':' -f2 | xargs)
VERSION="v${VERSION}"

helm package --version "${VERSION}" "${CHART}"
